import { Module } from '@nestjs/common';

import { ProductsModule } from './products/products.module';
import { OrdersModule } from './orders/orders.module';

import { CustomersModule } from './customers/customers.module';
import { GalleriesModule } from './galleries/galleries.module';
import { CategoriesModule } from './categories/categories.module';
import { SubcategoriesModule } from './subcategories/subcategories.module';
import { DeliveriesModule } from './deliveries/deliveries.module';
import { UsersModule } from './users/users.module';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from '@nestjs/config';
import { ProvidersModule } from './providers/providers.module';
import { AutheModule } from './authe/authe.module';

@Module({
  imports: [ 
    ConfigModule.forRoot(),
    MongooseModule.forRoot('mongodb://127.0.0.1:27017/myback'),
    ProductsModule, OrdersModule, CustomersModule, GalleriesModule, CategoriesModule, SubcategoriesModule, DeliveriesModule, UsersModule, AuthModule, ProvidersModule,],


})
export class AppModule {}
