
import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { HydratedDocument } from "mongoose";
import { Customer } from "src/customers/entities/customer.entity";
import * as argon2 from 'argon2';
export type UserDocument = HydratedDocument<User>;
@Schema({ discriminatorKey: 'items' })
export class User {
 @Prop({ type: String, required: true, enum: [Customer.name] })
  items: string
  @Prop({ required: true })
  firstname: string;
  @Prop({ required: true })
  lastname: string;
  @Prop({ required: true, unique: true })
  username: string;
  @Prop({ required: true })
  password: string;
  @Prop({ required: true, unique: true })
  email: string;
  @Prop({ required: true, unique: true })
  mobile: string;
  @Prop({ })
  refreshToken: string;

}
export const UserSchema = SchemaFactory.createForClass(User).pre("save", async function () {
    this.password = await argon2.hash(this.password);
  });