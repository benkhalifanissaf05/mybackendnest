import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import {UserDocument } from './entities/user.entity';
import { IUser } from './interfaces/user.interface';

@Injectable()
export class UsersService {

  constructor(@InjectModel('users') private usersModel: Model<UserDocument>) {}

 async create(createUserDto: CreateUserDto) : Promise <IUser>{
  const creatuser = new this.usersModel(createUserDto);
  return creatuser.save();
  }

  async findAll() : Promise <IUser[]>{
    return this.usersModel.find().select('-password').select('-__v').exec();
  }

  async findOne(username: string) : Promise <IUser>{
    return this.usersModel.findOne({username: username}).select('-__v').exec();
  }
  
  async findMobile(mobile: string) : Promise <IUser>{
    console.log(mobile);
    return this.usersModel.findOne({mobile: mobile}).select('-__v').exec();
  }
  async findByid(id: string) {
    return this.usersModel.findById(id).select('-__v').exec();;
  }
  async update(id: string, updateUserDto: UpdateUserDto): Promise <IUser> {
    return this.usersModel.findByIdAndUpdate(id, updateUserDto, { new: true }).select('-__v').exec();
    }

  remove(id: string) {
    return this.usersModel.findByIdAndDelete(id,{new: true}).select('-__v').exec();
  }
}
