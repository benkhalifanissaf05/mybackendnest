import { Controller, Get, Post, Body, Patch, Param, Delete, Query, Res,UseGuards} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
//import { _JwtAuthGuard } from 'src/authe/guards/jwt-auth.guard';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}
  @Get('username')
 async  findOne( @Res() response ,@Query('username') username: string) {

    try {
      const data=await this.usersService.findOne(username);
      return response.status(200).json({
         status:"200",
         message:"successfully",
         data: data
     
      });
     } catch (err) {
      return response.status(500).json({
       status:"500",
       message:err.message,
       data: null
     });
       }

}

@Get('mobile')
async  findMobile(@Res () response, @Query('mobile') mobile: string) {
  

 try {
  const data =await this.usersService.findMobile(mobile);
 return response.status(200).json({
    status:"200",
    message:"successfully",
    data: data

 });
} catch (err) {
 return response.status(500).json({
  status:"500",
  message:err.message,
  data: null
});
  }
 }
  @Post()
  async create(@Res () response ,@Body() createUserDto: CreateUserDto) {

   try {
    const items= await  this.usersService.create(createUserDto);
    return response.status(200).json({
       status:"200",
       message:"successfully",
       data: {
        username: items.username,
        lastname: items.lastname,
        firstname: items.firstname,
        mobile: items.mobile,
        email: items.email,


      }
   
    });
   } catch (err) {
    return response.status(500).json({
     status:"500",
     message:err.message,
     data: null
   });
     }
  }
 // @UseGuards(_JwtAuthGuard)
  @Get()
 async  findAll(@Res() response) {

try {
  const data= await this.usersService.findAll();
  if (data.length==0 ) {
    return response.status(200).json({
      status:"200",
      message:"empty user list",
      data: data}
    )}  
  
 return response.status(200).json({
    status:"200",
    message:"liste of users successfully",
    data: data

 });
} catch (err) {
 return response.status(500).json({
  status:"500",
  message:err.message,
  data: null
});
  }
 }
 @Get(':id')
 async findByid(@Res() response ,@Param('id') id: string) {
  const data=await this.usersService.findByid(id);
   try {
     /*if (data==null) {
       return response.status(500).json({
         status:"500",
         message:"not found",
         data: data}
       )}  */
  
    return response.status(200).json({
       status:"200",
       message:"successfully",
       data: data
   
    });
   } catch (err) {
    return response.status(500).json({
     status:"500",
     message:err.message,
     data: null
   });
     }
    }
  @Patch(':id')
  async update(@Res() response, @Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {

    try {
      const data =await this.usersService.update(id, updateUserDto);
      response.status(200).json({
        status:"200",
        message:"user updated successfully",
        data: data
      })
      
    } catch (error) {
      response.status(500).json({
        status:"500",
        message:error.message,
        data: null
      })
      
    }
  }

  @Delete(':id')
  async remove(@Res() response ,@Param('id') id: string) {

   try {
    const data= await this.usersService.remove(id);
    response.status(200).json({
      status:"200",
      message:"user deleted successfully",
      data: data
    })
   } catch (error) {
    response.status(500).json({
      status:"500",
      message:error.message,
      data: null
    })

   }
  }

}