import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { subCategoyDocument } from 'src/subcategories/entities/subcategory.entity';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { ProductDocument } from './entities/product.entity';
import { ProductI } from './interfaces/products.interface';

@Injectable()
export class ProductsService {
  constructor(@InjectModel('products') private ProductModel: Model<ProductDocument>,
  @InjectModel('subcategories') private SubCategoryModel: Model<subCategoyDocument>)
  { }
  async create(createProductDto: CreateProductDto): Promise<ProductI> {

    const creatProduct = new this.ProductModel(createProductDto);
    console.log("productssss");
   await  this.SubCategoryModel.findOneAndUpdate(createProductDto.subcategory , { $push: { products: creatProduct._id } })
       return creatProduct.save().then( creatProduct =>creatProduct.populate({path: "subcategory", select: ('-__v ')}));

  }

  async findAll() {
    return await this.ProductModel.find().select('-__v').exec();
  }

  async findOne(id: string) {
    return this.ProductModel.findById(id).select('-__v').exec();
  }

  update(id: string, updateProductDto: UpdateProductDto) {
    return this.ProductModel.findByIdAndUpdate({ _id: id }, updateProductDto, { new: true }).select('-__v').exec();
  }

  remove(id: string) {
    return this.ProductModel.findByIdAndDelete({ _id: id }, { new: true }).select('-__v').exec();
    
  }
  async findOneByName(ref:string)  {
    return await this.ProductModel.findOne({_ref:ref}).select('-__v').exec();
  }
 
}/*
removeall() {
  return this.ProductModel.deleteMany().exec();
}*/