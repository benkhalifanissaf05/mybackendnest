import { Controller, Get, Post, Body, Patch, Param, Delete, Res, Query } from '@nestjs/common';
import { ProductsService } from './products.service';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Response } from 'express';

@Controller('products')
export class ProductsController {
  constructor(private readonly ProductsService: ProductsService) { }


  @Post()
  async create(@Body() createProductDto: CreateProductDto, @Res() response: Response) {

    try {
      const data = await this.ProductsService.create(createProductDto);
      console.log("products");
      
      response.status(200).json({
        status: '200',
        message: "Product created successfully",
        data: {
          // id: data.id,
          price: data.price,
          description: data.description,
          ref: data.ref,
          qte: data.qte,

          subcategory: data.subcategory
        }// populate 
      });

    } catch (error) {
      response.status(500).json({
        status: '500',
        message: "failed : " + error.message,
        data: null
      });
    }
  }


  @Get()
  async findAll(@Res() response: Response) {


    try {

      const item = await this.ProductsService.findAll();
      response.status(201).json({
        status: 201,
        message: " all products ",
        data: item
      })

    } catch (err) {
      return response.status(400).json({
        status: 400,
        message: err.message,
        data: null
      })
    }
  }

  @Get('getbyid/:id')
  async findOne(@Param('id') id: string, @Res() response: Response) {



    try {

      const item = await this.ProductsService.findOne(id);
      response.status(201).json({
        status: 201,
        message: "product fetshed",
        data: item
      })

    } catch (err) {
      return response.status(400).json({
        status: 400,
        message: err.message,
        data: null
      })
    }
  }

  @Patch(':id')
  async update(@Res() response: Response, @Param('id') id: string, @Body() updateProductDto: UpdateProductDto) {

    try {

      const item = await this.ProductsService.update(id, updateProductDto);
      response.status(201).json({
        status: 201,

        message: "product updated",
        data: item
      })

    } catch (err) {
      return response.status(400).json({
        status: 400,
        message: err.message,
        data: null
      })
    }
  }

  @Delete('delette/id')
  async remove(@Param('id') id: string, @Res() response: Response) {

    try {

      const item = await this.ProductsService.remove(id);
      response.status(201).json({
        status: 201,

        message: "product delleted",
        data: item
      })

    } catch (err) {
      return response.status(400).json({
        status: 400,
        message: err.message,
        data: null
      })
    }
  }
  @Get('getref')
  async findOneByName(@Query('ref') ref: string, @Res() response : Response ) {


    try {

      const item = await this.ProductsService.findOneByName(ref);
      response.status(201).json({
        status: 201,
        message: "product fetshed",
        data: item
      })

    } catch (err) {
      return response.status(400).json({
        status: 400,
        message: err.message,
        data: null
      })
    }
  }
}