import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument, Types } from 'mongoose';
export type ProductDocument = HydratedDocument<Product>;
@Schema()
export class Product {
    @Prop({ required: true })
    price: string;
    @Prop({ required: true })
    ref: string;
    @Prop({ required: true })
    qte: string;
    @Prop({ required: true })
    description: string;
    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'subcategories' })
    subcategory: Types.ObjectId;
    @Prop([{ type: mongoose.Schema.Types.ObjectId, ref: 'providers' }])
    provider: Types.ObjectId;
}
export const ProductSchema = SchemaFactory.createForClass(Product);