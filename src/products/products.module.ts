import { Module } from '@nestjs/common';
import { ProductsService } from './products.service';
import { ProductsController } from './products.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { ProductSchema } from './entities/product.entity';
import { subCategoySchema } from 'src/subcategories/entities/subcategory.entity';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: "products", schema: ProductSchema }]),
     MongooseModule.forFeature([{name: 'subcategories', schema: subCategoySchema}],  
    )],
  controllers: [ProductsController],
  providers: [ProductsService]
})
export class ProductsModule {}
