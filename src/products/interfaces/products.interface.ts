import { Types } from "mongoose";

export interface ProductI {
    price: string;
    ref: string;
    qte: string;
    description: string;
    subcategory: Types.ObjectId;
    provider: Types.ObjectId;
    orders: Types.ObjectId;
    galleries: Types.ObjectId;
}