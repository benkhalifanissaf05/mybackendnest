import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, Request } from '@nestjs/common';
import { AuthService } from './auth.service';
import { CreateAuthDto } from './dto/create-auth.dto';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { LocalAuthGuard } from './guards/local-auth.guard';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Request() req) {

    return this.authService.login(req.user);
  }

  //@UseGuards(JwtAuthGuard)
  @Get()
  async getall(@Request() req) {
    return "zzz";
  } 
  @Post('refresh')
  async refresh(@Request() req,) {
    return await this.authService.refreshToken(req.refreshToken)
  }

}

