import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import * as argon2 from 'argon2';
import { stringify } from 'querystring';
import * as randomToken from 'rand-token';
import { User } from 'src/users/entities/user.entity';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
@Injectable()
export class AuthService {


  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,

  ) { }

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersService.findOne(username);

    console.log("validate user service");
    const { password, ...result } = user; const passwordMatches = await argon2.verify(user.password, pass); if (user && passwordMatches) {
      const { password, ...result } = user;
      return result;
    }
  }

  async login(user: any) {

    const payload = { username: user.username, sub: user.userId };
    const ref = await this.jwtService.sign(payload)
    const decodedToken = this.jwtService.decode(ref);
    const refreshToken= await this.generateRefreshToken({id:user.userId})
    console.log('login service ');
    return {

      access_token: this.jwtService.sign(payload
      ),
      refreshToken
    //  decodedToken

    };
  }
  async generateAccesToken(user: any) {

    const payload = { username: user.username, sub: user.userId };
   
    return {

      access_token: this.jwtService.sign(payload
      ),
   
    //  decodedToken

    };
  }
  async generateRefreshToken(user: any) {
    const refreshToken = this.jwtService.sign(user,
      {
        secret: process.env.REFRESH_TOKEN_SECRET,
        expiresIn: '1y'
      });

    return {
      refreshToken

    }





    

  }



  async refreshToken(token: string) {

    try {
      var decodeToken: any = this.jwtService.verify(token)
      console.log("decode");
      
    } catch(err) {
      throw new UnauthorizedException()
      console.log("no decode");
      
    }

    let user = await this.usersService.findByid(decodeToken.sub)

    let a= await this.usersService.findOne(token)
   // const refresh= await this.usersService.findOne(userId);
    if(!a) throw new UnauthorizedException()

    const accessToken = await this.generateAccesToken({ id: user.id, username: user.username })
    const refreshToken = await this.generateRefreshToken({ id: user.id })

      this.jwtService.decode(token)
    

    return {
      user,
      accessToken,
      refreshToken
    }
  }

}





