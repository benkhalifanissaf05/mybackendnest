import { Controller, Get, Post, Body, Patch, Param, Delete, Res, Query } from '@nestjs/common';
import { SubcategoriesService } from './subcategories.service';
import { CreateSubcategoryDto } from './dto/create-subcategory.dto';
import { UpdateSubcategoryDto } from './dto/update-subcategory.dto';
import { Response } from 'express';

@Controller('subcategories')
export class SubcategoriesController {
  constructor(private readonly SubcategoriesService: SubcategoriesService) {}
 
  @Post()
  async create(@Body() createSubcategoryDto: CreateSubcategoryDto, @Res() response: Response) {

    try {
      const data =await this.SubcategoriesService.create(createSubcategoryDto);
      response.status(201).json({
        status:'201',
        message:"subcategory created successfully",
        data: {
          // id: data.id,
            name: data.name,
            description: data.description,
           category: data.category
        }// populate 
      });
      
     }catch(error){
      response.status(500).json({
        status:'500',
        message:"failed : "+ error.message,
        data: null
      });
     }
      }
     

  @Get()
  findAll() {
    return this.SubcategoriesService.findAll();
  }

  @Get('getbyid/:id')
  findOne(@Param('id') id: string) {
    return this.SubcategoriesService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateSubcategoryDto: UpdateSubcategoryDto) {
    return this.SubcategoriesService.update(id, updateSubcategoryDto);
  }

  @Delete('delette/id')
  remove(@Param('id') id: string) {
    return this.SubcategoriesService.remove(id);
  }
  @Get('getname')
  findOneByName(@Query('name') name: string) {

    console.log("name of ", name)
    return this.SubcategoriesService.findOneByName(name);
  }
 
}
/* @Delete('remove')
  removeAll() {
    return this.SubcategoriesService.removeall;
  } */