import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument, Types } from 'mongoose';
export type subCategoyDocument = HydratedDocument<subCategoy>;
@Schema()
export class subCategoy {
  @Prop({required:true})
  name: string;
  @Prop({required:true})
  description: string;
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'categories', required: true }) 
  category: Types.ObjectId;
  @Prop([{ type: mongoose.Schema.Types.ObjectId, ref: 'products' }])
  products: Types.ObjectId
}
export const subCategoySchema = SchemaFactory.createForClass(subCategoy);