import { Module } from '@nestjs/common';
import { SubcategoriesService } from './subcategories.service';
import { SubcategoriesController } from './subcategories.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { subCategoySchema } from './entities/subcategory.entity';
import { CategorySchema } from 'src/categories/entities/category.entity';

@Module({
  imports:[MongooseModule.forFeature([{name: 'subcategories', schema: subCategoySchema}],  
  ),MongooseModule.forFeature([{name:'categories', schema: CategorySchema}])],
  controllers: [SubcategoriesController],
  providers: [SubcategoriesService]
})
export class SubcategoriesModule {}

