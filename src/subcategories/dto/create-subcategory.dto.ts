import { Types } from "mongoose";

export class CreateSubcategoryDto {
    name: string;
    description: string
    category: Types.ObjectId;
    products: Types.ObjectId
}
