import { Types } from "mongoose";

export interface subCategoryI {
    name: string;
    description: string,
    category: Types.ObjectId,
    products: Types.ObjectId
   
}