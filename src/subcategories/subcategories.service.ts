import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CategoryDocument } from 'src/categories/entities/category.entity';
import { CreateSubcategoryDto } from './dto/create-subcategory.dto';
import { UpdateSubcategoryDto } from './dto/update-subcategory.dto';
import { subCategoyDocument } from './entities/subcategory.entity';
import { subCategoryI } from './interfaces/subcategory.interface';


@Injectable()
export class SubcategoriesService  {
  constructor(@InjectModel('subcategories') private SubCategoryModel: Model<subCategoyDocument>,
  @InjectModel('categories') private CategoryModel: Model<CategoryDocument>)
  { }
  async create(createSubcategoryDto: CreateSubcategoryDto): Promise<subCategoryI> {

    const creatsubcategory = new this.SubCategoryModel(createSubcategoryDto);

   await  this.CategoryModel.findOneAndUpdate(createSubcategoryDto.category , { $push: { subcategories: creatsubcategory._id } })
       return creatsubcategory.save().then( creatsubCategory =>creatsubCategory.populate({path: "category products", select: ('-__v -_id -subcategories')}));

  }

  async findAll(): Promise<subCategoryI[]> {
    return this.SubCategoryModel.find().populate({path: "category products", select: ('-__v -_id  -__v')}).exec();
  }

  async findOne(id: string) {
    return this.SubCategoryModel.findById(id).select('-__v').exec();
  }

  update(id: string, updateSubcategoryDto: UpdateSubcategoryDto) {
    return this.SubCategoryModel.findByIdAndUpdate({ _id: id }, updateSubcategoryDto, { new: true }).select('-__v').exec();
  }

  remove(id: string) {
    return this.SubCategoryModel.findByIdAndDelete({ _id: id }, { new: true }).select('-__v').exec();
    
  }
  async findOneByName(name:string) : Promise<subCategoryI> {
    return await this.SubCategoryModel.findOne({name:name}).select('-__v').exec();
  }
  removeall() {
    return this.SubCategoryModel.deleteMany().exec();
  }
}
