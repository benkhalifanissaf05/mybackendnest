import { PartialType } from '@nestjs/mapped-types';
import { CreateNissafDto } from './create-nissaf.dto';

export class UpdateNissafDto extends PartialType(CreateNissafDto) {}
