import { Controller, Get, Post, Body, Patch, Param, Delete, Res, Query } from '@nestjs/common';
import { NissafsService } from './Nissafs.service';
import { CreateNissafDto } from './dto/create-Nissaf.dto';
import { UpdateNissafDto } from './dto/update-Nissaf.dto';
import { Response } from 'express';

@Controller('Nissafs')
export class NissafsController {
  constructor(private readonly NissafsService: NissafsService) { }

  
  @Post()
  async create(@Body() createNissafDto: CreateNissafDto, @Res() response: Response) {

    try {
      const data = await this.NissafsService.create(createNissafDto);
      console.log("Nissafs");
      
      response.status(200).json({
        status: '200',
        message: "Nissaf created successfully",
        data: {
          // id: data.id,
          adresse:data.adresse,

          products: data.products
        }// populate 
      });

    } catch (error) {
      response.status(500).json({
        status: '500',
        message: "failed : " + error.message,
        data: null
      });
    }
  }


  @Get()
  async findAll(@Res() response: Response) {


    try {

      const item = await this.NissafsService.findAll();
      response.status(201).json({
        status: 201,
        message: " all Nissafs ",
        data: item
      })

    } catch (err) {
      return response.status(400).json({
        status: 400,
        message: err.message,
        data: null
      })
    }
  }

  @Get('getbyid/:id')
  async findOne(@Param('id') id: string, @Res() response: Response) {



    try {

      const item = await this.NissafsService.findOne(id);
      response.status(201).json({
        status: 201,
        message: "Nissaf fetshed",
        data: item
      })

    } catch (err) {
      return response.status(400).json({
        status: 400,
        message: err.message,
        data: null
      })
    }
  }

  @Patch(':id')
  async update(@Res() response: Response, @Param('id') id: string, @Body() updateNissafDto: UpdateNissafDto) {

    try {

      const item = await this.NissafsService.update(id, updateNissafDto);
      response.status(201).json({
        status: 201,

        message: "Nissaf updated",
        data: item
      })

    } catch (err) {
      return response.status(400).json({
        status: 400,
        message: err.message,
        data: null
      })
    }
  }

  @Delete('delette/id')
  async remove(@Param('id') id: string, @Res() response: Response) {

    try {

      const item = await this.NissafsService.remove(id);
      response.status(201).json({
        status: 201,

        message: "Nissaf delleted",
        data: item
      })

    } catch (err) {
      return response.status(400).json({
        status: 400,
        message: err.message,
        data: null
      })
    }
  }
  @Get('getref')
  async findOneByName(@Query('ref') ref: string, @Res() response : Response ) {


    try {

      const item = await this.NissafsService.findOneByName(ref);
      response.status(201).json({
        status: 201,
        message: "Nissaf fetshed",
        data: item
      })

    } catch (err) {
      return response.status(400).json({
        status: 400,
        message: err.message,
        data: null
      })
    }
  }
}