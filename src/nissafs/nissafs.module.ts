import { Module } from '@nestjs/common';
import { NissafsService } from './nissafs.service';
import { NissafsController } from './nissafs.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { NissafSchema } from './entities/Nissaf.entity';

@Module({
  imports: [ MongooseModule.forFeature([{ name: "nissafs", schema: NissafSchema }]), ],
  controllers: [NissafsController],
  providers: [NissafsService]
})
export class NissafsModule {}
