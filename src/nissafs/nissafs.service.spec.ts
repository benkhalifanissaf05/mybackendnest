import { Test, TestingModule } from '@nestjs/testing';
import { NissafsService } from './nissafs.service';

describe('NissafsService', () => {
  let service: NissafsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [NissafsService],
    }).compile();

    service = module.get<NissafsService>(NissafsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
