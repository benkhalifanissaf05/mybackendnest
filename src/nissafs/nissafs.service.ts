import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateNissafDto } from './dto/create-Nissaf.dto';
import { UpdateNissafDto } from './dto/update-Nissaf.dto';
import {  NissafDocument } from './entities/Nissaf.entity';
import { NissafI } from './interfaces/nissaf.interface';

@Injectable()
export class NissafsService {
  constructor(@InjectModel('nissafs') private NissafModel: Model<NissafDocument>,
  /*@InjectModel('products') private ProductgoryModel: Model<subCategoyDocument>*/)
  { }
  async create(createNissafDto: CreateNissafDto): Promise<NissafI> {

    const creatNissaf = new this.NissafModel(createNissafDto);
    console.log("Nissafssss");
  // await  this.SubCategoryModel.findOneAndUpdate(createNissafDto.subcategory , { $push: { Nissafs: creatNissaf._id } })
       return creatNissaf.save().then( creatNissaf =>creatNissaf.populate({path: "products", select: ('-__v ')}));

  }

  async findAll() {
    return await this.NissafModel.find().select('-__v').exec();
  }

  async findOne(id: string) {
    return this.NissafModel.findById(id).select('-__v').exec();
  }

  update(id: string, updateNissafDto: UpdateNissafDto) {
    return this.NissafModel.findByIdAndUpdate({ _id: id }, updateNissafDto, { new: true }).select('-__v').exec();
  }

  remove(id: string) {
    return this.NissafModel.findByIdAndDelete({ _id: id }, { new: true }).select('-__v').exec();
    
  }
  async findOneByName(ref:string)  {
    return await this.NissafModel.findOne({_ref:ref}).select('-__v').exec();
  }
 
}/*
removeall() {
  return this.NissafModel.deleteMany().exec();
}*/