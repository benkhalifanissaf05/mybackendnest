import { Test, TestingModule } from '@nestjs/testing';
import { NissafsController } from './nissafs.controller';
import { NissafsService } from './nissafs.service';

describe('NissafsController', () => {
  let controller: NissafsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NissafsController],
      providers: [NissafsService],
    }).compile();

    controller = module.get<NissafsController>(NissafsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
