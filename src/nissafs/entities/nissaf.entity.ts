import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument, Types } from 'mongoose';
export type NissafDocument = HydratedDocument<Nissaf>;
@Schema()
export class Nissaf {
    @Prop({ required: true })
  
   adresse: string;
   @Prop([{ type: mongoose.Schema.Types.ObjectId, ref: 'products' }])
   products: Types.ObjectId
}
export const NissafSchema = SchemaFactory.createForClass(Nissaf);
