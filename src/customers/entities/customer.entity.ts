import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose, { HydratedDocument, Types } from "mongoose";
import { type } from "os";

export type CustomerDocument = HydratedDocument<Customer>;
@Schema()
export class Customer {

    items: string;
    time: Date;


    @Prop({ required: true })
    localization: string;
    // @Prop([{ type: mongoose.Schema.Types.ObjectId, ref: 'orders' }])
    //orders: Types.ObjectId;
}
export const CustomerSchema = SchemaFactory.createForClass(Customer);