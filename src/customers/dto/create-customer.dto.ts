import { Types } from "mongoose";

export class CreateCustomerDto {
    items:string;
    time:Date;
    localization: string;
    orders: Types.ObjectId;
}
