import { Controller, Get, Post, Body, Patch, Param, Delete, Res, Query } from '@nestjs/common';
import { Response } from 'express';
import { CustomersService } from './customers.service';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { UpdateCustomerDto } from './dto/update-customer.dto';

@Controller('customers')
export class CustomersController {
  constructor(private readonly CustomerService: CustomersService) {}

  @Post()
  async create(@Body() createCustomerDto: CreateCustomerDto, @Res() response: Response) {

    try {
      const data =await this.CustomerService.create(createCustomerDto);
      response.status(201).json({
        status:'201',
        message:"Customer created successfully",
        data: {
          // id: data.id,
          localization: data.localization,
          //orders: data.orders
        }// populate 
      });
      
     }catch(error){
      response.status(500).json({
        status:'500',
        message:"failed : "+ error.message,
        data: null
      });
     }
      }
     

  @Get()
 async  findAll(@Res() response : Response ) {
  try {
    const data =await  this.CustomerService.findAll();
  response.status(201).json({
      status:'201',
      message:"all liste of Customer  ",
      data: data
    });
    
   }catch(error){
    response.status(500).json({
      status:'500',
      message:"failed : "+ error.message,
      data: null
    });
   }
  }

  @Get('getbyid/:id')
  findOne(@Param('id') id: string) {
    return this.CustomerService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCustomerDto: UpdateCustomerDto) {
    return this.CustomerService.update(id, updateCustomerDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.CustomerService.remove(id);
  }
  @Get('getname')
  findOneByName(@Query('name') name: string) {

    console.log("name of ", name)
    return this.CustomerService.findOneByName(name);
  }
}
