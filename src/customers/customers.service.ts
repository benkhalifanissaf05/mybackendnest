import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { UpdateCustomerDto } from './dto/update-customer.dto';
import { CustomerDocument } from './entities/customer.entity';
import { CustomerI } from './interfaces/customers.interface';

@Injectable()
export class CustomersService {
  constructor(@InjectModel('customers') private CustomerModel: Model<CustomerDocument>)
  { }
  async create(createCustomerDto: CreateCustomerDto): Promise<CustomerI> {

    const creatCustomer = new this.CustomerModel(createCustomerDto);
       return creatCustomer.save();
       //.then( creatCustomer =>creatCustomer.populate({path: "orders", select: ('-__v -_id -customers')}));

  }

  async findAll(): Promise<CustomerI[]> {
    return this.CustomerModel.find().select('-_v').exec();
  }

  async findOne(id: string) {
    return this.CustomerModel.findById(id).exec();
  }

  update(id: string, updateCustomerDto: UpdateCustomerDto) {
    return this.CustomerModel.findByIdAndUpdate({ _id: id }, updateCustomerDto, { new: true });
  }

  remove(id: string) {
    return this.CustomerModel.findByIdAndDelete({ _id: id }, { new: true });
  }
  async findOneByName(name:string) : Promise<CustomerI> {
    return await this.CustomerModel.findOne({name:name}).exec();
  }
}
