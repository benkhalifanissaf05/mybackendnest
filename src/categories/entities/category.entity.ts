import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument, Types } from 'mongoose';
export type CategoryDocument = HydratedDocument<Category>;
@Schema()
export class Category {
  @Prop({required:true})
  name: string;
  @Prop({required:true})
  description: string;
  @Prop([{ type: mongoose.Schema.Types.ObjectId, ref: 'subcategories' }]) 
  subcategories:Types.ObjectId;
}
export const CategorySchema = SchemaFactory.createForClass(Category);