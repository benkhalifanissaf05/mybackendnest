import { Controller, Res, Get, Put, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common';
import { Response } from 'express';
import { CategoriesService } from './categories.service';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';

@Controller('categories')
export class CategoriesController {
  constructor(private readonly categoriesService: CategoriesService) { }

  @Post()
  async create(@Res() response, @Body() createCategoryDto: CreateCategoryDto) {
    try {

      const item = await this.categoriesService.create(createCategoryDto);
      return response.status(201).json({
        status: 201,
        message: "created",
        data: {
          name: item.name,
          description: item.description,
          subcategories:item.subcategories
        }
      })

    } catch (err) {
      return response.status(400).json({
        status: 400,
        message: err.message,
        data: null
      })
    }

  }
  @Get('getname')
 async  findOneByName(@Query('name') name: string, @Res() response: Response) {

    try {

      const item = await this.categoriesService.findOneByName(name);
      response.status(201).json({
        status: 201,
        message: "category fetshed",
        data: item
      })

    } catch (err) {
      return response.status(400).json({
        status: 400,
        message: err.message,
        data: null
      })
    }
  }

  @Patch(':id')
 async  update(@Param('id') id: string, @Body() updateCategoryDto: UpdateCategoryDto, @Res() response: Response) {
  
    try {

      const item = await this.categoriesService.update(id, updateCategoryDto);
      response.status(201).json({
        status: 201,

        message: "category updated",
        data: item
      })

    } catch (err) {
      return response.status(400).json({
        status: 400,
        message: err.message,
        data: null
      })
    }
  }

  @Delete(':id')
  async remove(@Param('id') id: string, @Res() response: Response) {
try {
const  item = await this.categoriesService.remove(id);
response.status(201).json({
         status: 201,
         message: "category deltted",
        data: item
      })

    } catch (err) {
      return response.status(400).json({
        status: 400,
        message: err.message,
        data: null
      })
    }
  }
  @Get('/getbyid/:id')
 async findOne(@Param('id') id: string, @Res() response: Response) {
   try{ 
   const  item = await this.categoriesService.findOne(id);
    response.status(201).json({
      status: 201,
      message: "category fetshed",
      data: item
    })

  } catch (err) {
    return response.status(400).json({
      status: 400,
      message: err.message,
      data: null
    })
  }
  }

  @Get()
  async findAll(@Res() response: Response) {

  

    try {
      const items = await this.categoriesService.findAll();
      return response.status(200).json({
        status: 200,
        message: "find all item categories",
        data: items
      })

    } catch (err) {
      return response.status(400).json({
        status: 400,
        message: err.message,
        data: null
      })
    }
  }
}
