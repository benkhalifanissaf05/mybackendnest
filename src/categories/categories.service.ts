import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { CategoryDocument } from './entities/category.entity';
import { Category } from './interfaces/category.interface';

@Injectable()
export class CategoriesService {

  constructor(@InjectModel("categories") private categoryModel: Model<CategoryDocument>) {}

  async create(createCategoryDto: CreateCategoryDto) : Promise<Category> {
    const createdCategory = new this.categoryModel(createCategoryDto);
    const item=await createdCategory.save().then( creatCategory =>creatCategory.populate({path: "subcategories", select: ('-__v -_id -category')}));
    return item

  }
  async findAll(): Promise<Category[]> {
    return this.categoryModel.find().select("-__v").exec();
  }

  async findOne(id: string) : Promise<Category> {
    return this.categoryModel.findOne({id:id}).exec();
  }

  update(id: string, updateCategoryDto: UpdateCategoryDto) : Promise<Category> {
    console.log("id of update : ",id);
    return this.categoryModel.findByIdAndUpdate({_id:id},updateCategoryDto,{new:true}).exec();
  }

  remove(id: string) : Promise<Category> {
    return this.categoryModel.findByIdAndDelete({_id:id},{new:true}).exec();
  }

  
  async findOneByName(name:string) : Promise<Category> {
    return await this.categoryModel.findOne({name:name}).exec();
  }

}
