import { Types } from "mongoose";

export interface Category {
    name: string;
    description: string
    subcategories: Types.ObjectId
    
}