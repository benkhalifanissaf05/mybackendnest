import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AutheService } from '../authe.service';


@Injectable()
export class _LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private AutheService: AutheService) {
    super();
  }

  async validate(username: string, password: string): Promise<any> {
    console.log(" validate strategy");
    console.log(username);
    console.log(password);
    const user = await this.AutheService.validateUser(username, password);

    if (!user) {
    
      throw new UnauthorizedException("not found");
    }
    return user;
  }
}