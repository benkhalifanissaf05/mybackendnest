import { Module } from '@nestjs/common';
import { AutheService } from './authe.service';
import { AutheController } from './authe.controller';
import { UsersModule } from 'src/users/users.module';
import { _LocalStrategy } from 'src/authe/strategy/local.strategy';
import { PassportModule } from '@nestjs/passport';
import { JwtModule,} from '@nestjs/jwt';
import { _JwtStrategy } from 'src/authe/strategy/jwt.strategy';
import * as dotenv from 'dotenv';
dotenv.config();

@Module({
  imports: [UsersModule, PassportModule, JwtModule.register({
    secret:process.env.ACCESS_TOKEN_SECRET,
    signOptions: { expiresIn: '1000s' },
  }) ],
  controllers: [AutheController],
 
  providers: [AutheService, _LocalStrategy, _JwtStrategy],
  exports: [AutheService],
  
})
export class AutheModule {}
