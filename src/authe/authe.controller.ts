import { Controller, Get, Post, Req, Request, Res ,UseGuards} from '@nestjs/common';
import { AutheService } from './authe.service';
import { _LocalAuthGuard,} from './guards/local-authe.guard';
import {  _JwtAuthGuard,} from './guards/jwt-auth.guard';
import { Response } from 'express';
//import { UploadedFiles, UseInterceptors } from '@nestjs/common/decorators';
//import { FileInterceptor } from '@nestjs/platform-express';

@Controller('authe')
export class AutheController {
 constructor(private readonly autheService: AutheService) {}

  @UseGuards( _LocalAuthGuard )
  @Post('login')
  async login(@Req() req, @Res() response: Response) {
    console.log("login authe controller");
    const data = await this.autheService.login(req.user)
    try{
      response.status(200).json({
        status:"200",
        message: 'Login Successful',

        data: data

      });
      
    }
    catch(err){
      response.status(500).json({
        status:"500",
        message: err.message,
        data: null
      });
    }
  }
  @UseGuards( _LocalAuthGuard )
  @ Get('profile')
  async getProfile(@Request() req,@Res({passthrough: true}) response: Response) {
   
console.log("profile");
const data = await this.autheService.login(req.user)
const jwt=await data.acces_token;
response.cookie('jwt', jwt,{ httpOnly:true});
console.log(jwt);

return jwt;

  }/*
  @Post('upload')
  @UseInterceptors(FileInterceptor('file'))
  uploadFile(@UploadedFiles() file: Express.Multer.File) {
    console.log(file);
  }*/
}
