import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument, Types } from 'mongoose';
export type ProviderDocument = HydratedDocument<Provider>;
@Schema()
export class Provider {
    @Prop({ required: true })
    adresse: string;
    @Prop([{ type: mongoose.Schema.Types.ObjectId, ref: 'products' }])
    products: Types.ObjectId
}
export const ProviderSchema = SchemaFactory.createForClass(Provider);
