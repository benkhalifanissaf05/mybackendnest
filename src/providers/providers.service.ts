import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateProviderDto } from './dto/create-Provider.dto';
import { UpdateProviderDto } from './dto/update-Provider.dto';
import {  ProviderDocument } from './entities/Provider.entity';
import { ProviderI } from './interfaces/Provider.interface';

@Injectable()
export class ProvidersService {
  constructor(@InjectModel("providers") private providerModel: Model<ProviderDocument>){ }
  async create(createProviderDto: CreateProviderDto): Promise<ProviderI> {

    const creatProvider = new this.providerModel(createProviderDto);
    // await  this.SubCategoryModel.findOneAndUpdate(createProviderDto.subcategory , { $push: { Providers: creatProvider._id } })
    return creatProvider.save().then(creatProvider => creatProvider.populate({ path: "subcategory", select: ('-__v ') }));

  }

  async findAll() {
    return await this.providerModel.find().select('-__v').exec();
  }

  async findOne(id: string) {
    return this.providerModel.findById(id).select('-__v').exec();
  }

  update(id: string, updateProviderDto: UpdateProviderDto) {
    return this.providerModel.findByIdAndUpdate({ _id: id }, updateProviderDto, { new: true }).select('-__v').exec();
  }

  remove(id: string) {
    return this.providerModel.findByIdAndDelete({ _id: id }, { new: true }).select('-__v').exec();

  }
  async findOneByName(ref: string) {
    return await this.providerModel.findOne({ _ref: ref }).select('-__v').exec();
  }

}/*
removeall() {
  return this.ProviderModel.deleteMany().exec();
}*/