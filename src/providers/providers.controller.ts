import { Controller, Get, Post, Body, Patch, Param, Delete, Res, Query } from '@nestjs/common';
import { ProvidersService } from './providers.service';
import { CreateProviderDto } from './dto/create-provider.dto';
import { UpdateProviderDto } from './dto/update-provider.dto';
import { Response } from 'express';

@Controller('providers')
export class ProvidersController {
  constructor(private readonly providersService: ProvidersService) {}

  @Post()
  async create(@Body() createProviderDto: CreateProviderDto, @Res() response: Response) {

    try {
      const data = await this.providersService.create(createProviderDto);
      console.log("Providers");
      
      response.status(200).json({
        status: '200',
        message: "Provider created successfully",
        data: {
          // id: data.id,
          adresse:data.adresse,

          products: data.products
        }// populate 
      });

    } catch (error) {
      response.status(500).json({
        status: '500',
        message: "failed : " + error.message,
        data: null
      });
    }
  }


  @Get()
  async findAll(@Res() response: Response) {


    try {

      const item = await this.providersService.findAll();
      response.status(201).json({
        status: 201,
        message: " all Providers ",
        data: item
      })

    } catch (err) {
      return response.status(400).json({
        status: 400,
        message: err.message,
        data: null
      })
    }
  }

  @Get('getbyid/:id')
  async findOne(@Param('id') id: string, @Res() response: Response) {



    try {

      const item = await this.providersService.findOne(id);
      response.status(201).json({
        status: 201,
        message: "Provider fetshed",
        data: item
      })

    } catch (err) {
      return response.status(400).json({
        status: 400,
        message: err.message,
        data: null
      })
    }
  }

  @Patch(':id')
  async update(@Res() response: Response, @Param('id') id: string, @Body() updateProviderDto: UpdateProviderDto) {

    try {

      const item = await this.providersService.update(id, updateProviderDto);
      response.status(201).json({
        status: 201,

        message: "Provider updated",
        data: item
      })

    } catch (err) {
      return response.status(400).json({
        status: 400,
        message: err.message,
        data: null
      })
    }
  }

  @Delete('delette/id')
  async remove(@Param('id') id: string, @Res() response: Response) {

    try {

      const item = await this.providersService.remove(id);
      response.status(201).json({
        status: 201,

        message: "Provider delleted",
        data: item
      })

    } catch (err) {
      return response.status(400).json({
        status: 400,
        message: err.message,
        data: null
      })
    }
  }
  @Get('getref')
  async findOneByName(@Query('ref') ref: string, @Res() response : Response ) {


    try {

      const item = await this.providersService.findOneByName(ref);
      response.status(201).json({
        status: 201,
        message: "Provider fetshed",
        data: item
      })

    } catch (err) {
      return response.status(400).json({
        status: 400,
        message: err.message,
        data: null
      })
    }
  }
}
