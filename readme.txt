Jeton d'accès : Il contient toutes les informations dont le serveur a besoin 
pour savoir si l'utilisateur / l'appareil peut accéder à la ressource que vous demandez ou non. 
Ce sont généralement des jetons expirés avec une courte période de validité.

Jeton d'actualisation : Le jeton d'actualisation est utilisé pour générer un nouveau jeton d'accès.
 En règle générale, si le jeton d'accès a une date d'expiration, une fois qu'il expire, l'utilisateur
  doit s'authentifier à nouveau pour obtenir un jeton d'accès. Avec le jeton d'actualisation, cette étape 
  peut être ignorée et avec une demande à l'API, obtenez un nouveau jeton d'accès qui permet à l'utilisateur
   de continuer à accéder aux ressources de l'application.

   Le jeton d'actualisation nécessite une plus grande sécurité lorsqu'il est stocké que le jeton d'accès, 
   car s'il était volé par des tiers, ils pourraient l'utiliser pour obtenir des jetons d'accès et accéder
    aux ressources protégées de l'application. Afin de couper un scénario comme celui-ci, un système doit être
     implémenté dans le serveur pour  invalider un jeton de rafraîchissement , en plus de définir une  durée de 
     vie  qui doit évidemment être plus longue que celle des jetons d'accès.